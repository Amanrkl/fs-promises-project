const fs = require("fs");
const path = require("path");

const dirPath = path.join(__dirname, "/randomJsonFiles");

function problem1() {
    return makeDirectory()
        .then(creatingFiles)
        .then(deletingFiles)
        .then(() => removeDirectory(dirPath));
}

function makeDirectory() {
    return new Promise(function (resolve, reject) {
        console.log("Creating directory started");

        fs.mkdir(dirPath, { recursive: true }, (err) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                console.log("Successfully created directory");
                resolve();
            }
        });
    });
}

function removeDirectory() {
    return new Promise(function (resolve, reject) {
        console.log("Removing directory started");

        fs.rmdir(dirPath, (err) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                console.log("Successfully deleted directory");
                resolve();
            }
        });
    });
}

function creatingFiles() {
    console.log("Starting json files creation");

    const noOfJSONFiles = Math.ceil(Math.random() * 10);
    const files = new Array(noOfJSONFiles).fill(0);


    return Promise.all(
        files.map((file, index) => {
            let fileName = `${index + 1}_file.json`;
            let filePath = path.join(dirPath, fileName);
            let fileData = {
                RandomNumber: Math.random(),
            };
            console.log(`Starting ${fileName} creation`);
            return new Promise(function (resolve, reject) {
                fs.writeFile(filePath, JSON.stringify(fileData), (err) => {
                    if (err) {
                        console.error(`Error when creating file ${fileName}`, err);
                        reject(err);
                    } else {
                        console.log(`Successfully created file ${fileName}`);
                        resolve(fileName);
                    }
                });
            })
        }
        )
    );

}

function deletingFiles(filenames) {
    console.log("Starting Deletion");

    return Promise.all(
        filenames.map((fileName) => {
            let filePath = path.join(dirPath, fileName);

            console.log(`Starting ${fileName} deletion`);

            return new Promise(function (resolve, reject) {
                fs.unlink(filePath, (err) => {
                    if (err) {
                        // console.error(`Error when deleting file ${fileName}`,err);
                        reject(err);
                    } else {
                        console.log(`Successfully deleted file ${fileName}`);
                        resolve();
                    }
                });
            });
        }),
    );
}

module.exports = problem1;
