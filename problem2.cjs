/*
    Problem 2:
    
    Using promises and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');

function problem2(file = "lipsum.txt", storingFile = "filenames.txt") {

    return readFile(file)
        .then((data) => writeFile(`upper${file}`, data.toUpperCase()))
        .then((fileName) => writeFile(storingFile, fileName))
        .then(() => readFile(`upper${file}`))
        .then((data) => {
            const updatedData = data.toLowerCase().replaceAll("\n", "").replaceAll(". ", ".").split(".").join('.\n');
            return writeFile(`lower${file}`, updatedData)
        })
        .then((fileName) => appendFile(storingFile, fileName))
        .then(() => readFile(`lower${file}`))
        .then((data) => {
            const sortedData = data.split("\n").sort().join("\n");
            return writeFile(`sorted${file}`, sortedData)
        })
        .then((fileName) => appendFile(storingFile, fileName))
        .then(() => readFile(storingFile))
        .then((data) => {
            const fileNames = data.split("\n");
            return Promise.all(
                fileNames.map(fileName => deleteFile(fileName))
            )
        })
        .then(() => deleteFile(storingFile))
}


function readFile(fileName) {
    return new Promise(function (resolve, reject) {
        const filePath = path.join(__dirname, fileName);
        console.log(`Starting to read ${fileName}`);
        fs.readFile(filePath, "utf-8", (err, data) => {
            if (err) {
                console.error(`Error in reading ${fileName}`, err);
                reject(err);
            } else {
                console.log(`Successful in reading ${fileName}`);
                resolve(data)
            }
        })
    });
}

function writeFile(fileName, dataToWrite) {
    return new Promise(function (resolve, reject) {
        const filePath = path.join(__dirname, fileName);
        console.log(`Starting to write in ${fileName}`);
        fs.writeFile(filePath, dataToWrite, (err) => {
            if (err) {
                console.error(`Error in writing in ${fileName}`, err);
                reject(err);
            } else {
                console.log(`Successful in writing in ${fileName}`);
                resolve(fileName)
            }
        })
    });
}

function appendFile(fileName, dataToAppend) {
    return new Promise(function (resolve, reject) {
        const filePath = path.join(__dirname, fileName);
        console.log(`Starting to append in ${fileName}`);
        fs.appendFile(filePath, "\n" + dataToAppend, (err) => {
            if (err) {
                console.error(`Error in appending to ${fileName}`, err);
                reject(err);
            } else {
                console.log(`Successful in appending to ${fileName}`);
                resolve(fileName)
            }
        })
    });
}

function deleteFile(fileName) {
    return new Promise(function (resolve, reject) {
        const filePath = path.join(__dirname, fileName);
        console.log(`Starting to delete  ${fileName}`);
        fs.unlink(filePath, (err) => {
            if (err) {
                console.error(`Error in deleting ${fileName}`, err);
                reject(err);
            } else {
                console.log(`Successful in deleting ${fileName}`);
                resolve(fileName)
            }
        })
    });
}

module.exports = problem2